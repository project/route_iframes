<?php

namespace Drupal\route_iframes\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalTasks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $config = \Drupal::config('route_iframes.routeiframesconfiguration');
    $name = $config->get('route_iframe_main_tab_name');
    $path = $config->get('route_iframe_main_tab_path');
    $tabs = $config->get('route_iframe_tabs');

    if (!empty($name) && !empty($path)) {
      // Create a root tab, which will redirect to the first accessible tab.
      $this->derivatives['route_iframes.root'] = $base_plugin_definition;
      $this->derivatives['route_iframes.root']['title'] = $name;
      $this->derivatives['route_iframes.root']['route_name'] = 'route_iframes.root';
      $this->derivatives['route_iframes.root']['base_route'] = 'entity.node.canonical';
      $weight = 0;
      if (!empty($tabs)) {
        foreach ($tabs as $tab) {
          $subtask = 'route_iframes.tab.' . $tab['path'];
          $this->derivatives[$subtask] = $base_plugin_definition;
          $this->derivatives[$subtask]['title'] = $tab['name'];
          $this->derivatives[$subtask]['route_name'] = $subtask;
          $this->derivatives[$subtask]['parent_id'] = 'route_iframes.dynamic_local_tasks:route_iframes.root';
          $this->derivatives[$subtask]['id'] = $subtask;
          $this->derivatives[$subtask]['weight'] = $weight;
          $this->derivatives[$subtask]['options'] = ['tab' => $tab['path']];
          $this->derivatives[$subtask]['base_route'] = 'entity.node.canonical';
          $weight++;
        }
      }
    }
    return $this->derivatives;
  }

}
