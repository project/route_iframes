<?php

namespace Drupal\route_iframes\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Route Iframe Configuration entities.
 */
interface RouteIframeConfigEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
