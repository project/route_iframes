<?php

namespace Drupal\route_iframes\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Drupal\token\Token;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class RouteIframeController.
 *
 * @package Drupal\route_iframes\Controller
 */
class RouteIframeController extends ControllerBase {

  /**
   * Drupal\token\Token definition.
   *
   * @var \Drupal\token\Token
   */
  protected $token;

  /**
   * Base url definition.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * Route Iframes sub-level tabs definition.
   *
   * @var array
   */
  protected $tabs;

  /**
   * Constructs a new RouteIframeController object.
   */
  public function __construct(Token $token) {
    $this->token = $token;
    $route_iframes_config = $this->config('route_iframes.routeiframesconfiguration');
    $this->baseUrl = $route_iframes_config->get('route_iframe_base_url');
    $this->tabs = $route_iframes_config->get('route_iframe_tabs');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token')
    );
  }

  /**
   * Build the iframe page.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The upcasted node object.
   * @param string $tab
   *   The active tab to check for matching configuration.
   *
   * @return array
   *   The iframe render array or a no match message render array.
   */
  public function build(NodeInterface $node, $tab) {
    $active = $this->getRouteConfig($node, $tab);
    if ($active) {
      // Attach the dashboard base_url to the configuration url.
      $config_url = '';
      if (!empty($this->baseUrl)) {
        $config_url = $this->token->replace($active->get('config'), ['node' => $node]);
        $config_url = $this->baseUrl . $config_url;
      }
      $iframe_height = (empty($active->get('iframe_height'))) ? 3000 : $active->get('iframe_height');
      return [
        '#theme' => 'route_iframe',
        '#config' => $config_url,
        '#iframe_height' => $iframe_height,
        '#cache' => [
          'max-age' => 0,
        ],
      ];
    }
    return [
      '#type' => 'markup',
      '#markup' => '<em>This page is not defined.</em>',
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Return content for the root tab.
   *
   * This will usually result in a redirect to the first available
   * tab on the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function buildRoot(NodeInterface $node) {
    foreach ($this->getActiveConfigurations($node) as $config) {
      $route_name = 'route_iframes.tab.' . $config->get('tab');
      return $this->redirect($route_name, ['node' => $node->id()]);
    }
    throw new NotFoundHttpException();
  }

  /**
   * Access callback for the "root" page.
   *
   * This access check just asserts that we have any active tab
   * within the current scope.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessRoot(NodeInterface $node) {
    $configs = $this->getActiveConfigurations($node);
    return AccessResult::allowedIf(!!$configs)
      ->addCacheTags(['config:route_iframe_config_entity_list']);
  }

  /**
   * A custom access method to check for config.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node object based on the route.
   * @param string $tab
   *   The tab that is being checked.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access object to grant or reject access.
   */
  public function accessTab(NodeInterface $node, $tab) {
    $active = $this->getRouteConfig($node, $tab);

    return AccessResult::allowedIf(!!$active)
      ->addCacheableDependency($active);
  }

  /**
   * Utility function to load config within scope.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check the scope against.
   * @param string $tab
   *   The tab that should be used in identifying the config.
   *
   * @return bool|\Drupal\Core\Entity\EntityInterface
   *   The route iframe config entity or False if not found.
   */
  private function getRouteConfig(NodeInterface $node, $tabId = '') {
    foreach ($this->getActiveConfigurations($node) as $config) {
      if ($config->get('tab') === $tabId) {
        return $config;
      }
    }
    return FALSE;
  }

  /**
   * Utility function to load all configurations active within a node's scope.
   *
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Drupal\route_iframes\Entity\RouteIframeConfigEntityInterface[]
   */
  private function getActiveConfigurations(NodeInterface $node) {
    $query = $this->entityTypeManager()->getStorage('route_iframe_config_entity')->getQuery();
    $query->sort('weight');

    $specificCondition = $query->andConditionGroup()
      ->condition('scope_type', 'specific')
      ->condition('scope', $node->id(), 'CONTAINS');

    $bundleCondition = $query->andConditionGroup()
      ->condition('scope_type', 'bundle')
      ->condition('scope', $node->bundle(), 'CONTAINS');

    $defaultCondition = $query->andConditionGroup()
      ->condition('scope_type', 'default');

    $matchingScopeCondition = $query->orConditionGroup()
      ->condition($specificCondition)
      ->condition($bundleCondition)
      ->condition($defaultCondition);

    $query->condition($matchingScopeCondition);

    $config_ids = $query->execute();

    return $this->entityTypeManager()
      ->getStorage('route_iframe_config_entity')
      ->loadMultiple($config_ids);
  }

}
